// ----------------------------NAVBAR--------------------------------------



let mainNavbar = document.querySelector('#mainNavbar');

window.addEventListener('scroll', () => {

    if (window.scrollY > 0) {

    mainNavbar.style.background = 'rgba(255, 250, 250, 0.9)';
} else {

    mainNavbar.style.background = 'rgb(255, 250, 250)';

}
})



// --------------------------CRAZY BUTTON-----------------------------------
var btn = document.querySelector('#BTN');
var position;

btn.addEventListener("mouseover", function() {

    position ? (position = 0) : (position = 150);
    btn.style.transform = `translate(${position}px,0px`;
    btn.style.transition = "all 0.3s ease";
});

// ----------------------ICON NAVBAR---------------------------

let iconNavbar = document.querySelector ("#iconNavbar")

let confirm = false




iconNavbar.addEventListener ('click', () => {
    
    if (confirm == false) {

    iconNavbar.style.transform ='rotate(180deg)'
    confirm = true

} else  {

iconNavbar.style.transform ='rotate(0deg)'
confirm = false

} })


// -------------------NUMERI COUNTER--------------------------



function createInterval (finalNumber, element) {

    let counter = 0

    let interval = setInterval ( () => {


        if (counter < finalNumber) {

            
            counter++

            element.innerHTML = counter;
          
        } else {

            clearInterval(interval)

        }

    }, 1);

}

let firstSpan = document.querySelector ('#firstSpan')
let secondSpan = document.querySelector ('#secondSpan')
let thirdSpan = document.querySelector ('#thirdSpan')


let h2Test = document.querySelector ('#h2Test')

let IntersectionInterval = true 

let observer = new IntersectionObserver (
    (entries)=> {

        entries.forEach ((entry) => {
            if (entry.isIntersecting && IntersectionInterval) {
            
                createInterval(1000, firstSpan, 2);
                createInterval(600, secondSpan, 1);
                createInterval(900, thirdSpan, 2.5);

                IntersectionInterval = false;

            }
        })


    }

)
observer.observe (h2Test)

//----------------------RECENSIONI CLIENTI---------------------







//---------------------------LAST PRODUCT --------------------------

let announcementsWrapper = document.querySelector('#announcementsWrapper')

let announcements = [

{name:'Mordecai', category : 'mascara', description: 'awesome', price: 19.90, image : './neauthy-skincare-8ZKwW-2SR28-unsplash.jpg'},

{name:'Blossom', category : 'gloss', description: 'awesome', price: 19.90, image : './header2.png'},

{name:'LipsLops', category : 'lipgloss', description: 'awesome',price: 19.90, image : './header.png' },

{name:'ContourMe', category : 'Countoring', description: 'awesome', price: 19.90, image: './neauthy-skincare-8jg7vumdUlU-unsplash.jpg' },

]

announcements.forEach( (annuncio, i) => {

   if (i >= (announcements.length - 4)) {


    let div = document.createElement('div')

    div.classList.add('col-12', 'col-md-6', 'col-lg-3', 'mt-5');

    div.innerHTML = `  
    
    <div class="card shadowcard pb-2 text-center">
        <img src=${annuncio.image} alt="...">

        <h3 class="card-title h3">${annuncio.name}</h3>
        <h4 class="card-description h4">${annuncio.description}</h4>
        <p class="card-text">${annuncio.price} €</p>

        <div class="d-flex justify-content-end mx-3" data-bs-toggle="modal" data-bs-target="#exampleModal">
          <a href="#" class=" btn btn-light">Dettagli</a>
        </div>


      </div>

    `

 announcementsWrapper.appendChild(div)
 
 
}

});

