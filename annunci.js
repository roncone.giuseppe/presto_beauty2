
// __________________________SCRIPT PAGINA ANNUNCI_______________________


// APERTURA FETCH PER ACCESSO A JSON
fetch('./annunci.json').then((response) => response.json()).then((data)=> {

    // CATTURO HTML
    let categoryWrapper = document.querySelector('#categoryWrapper');

    let cardWrapper = document.querySelector('#cardWrapper')


// FUNZIONE PER CREAZIONE RADIOBOTTOM FILTRANDO TUTTE LE CATEGORIE DI UN ARRAY
    function setCategoryFilters (){

    let categories = data.map((annuncio) => annuncio.category)

    let uniqueCategories = [];

    categories.forEach((category) => {

            if ( !uniqueCategories.includes(category)) {

                uniqueCategories.push(category)

             }
      
        })

        uniqueCategories.forEach((category) => {

            let div = document.createElement('div')

            div.classList.add ("form-check");

            div.innerHTML = `

            <input class="form-check-input" type="radio" checked name="flexRadioDefault" id="${category}">
                                <label class="form-check-label" for="${category}">
                                    ${category}
                                </label>
            
            `
            categoryWrapper.appendChild(div)

        })

    }

    setCategoryFilters();


// FUNZIONE PER CREAZIONE CARD
    function showCard (array) {

       cardWrapper.innerHTML = ''

       array.forEach((el) => {


            let div = document.createElement ('div')

            div.classList.add('announcementCard', 'card', 'mx-0', 'my-3', 'text-center', 'd-flex', 'flex-column', 'align-items-center', 'justify-content-evenly')

            div.innerHTML = `
            
            <h2 class="h3">${el.name}</h2>

                    <h4 class="h4">${el.category}</h4>

                    <p class="h5">${el.price}</p>
            
            
            `
            cardWrapper.appendChild(div)

       })



    }

    showCard(data)

// FUNZIONE PER FILTRI CATEGORIE
    function filteredByCategory(categoria) {

        if (categoria != 'all'){

        let filtered = data.filter((annuncio) => annuncio.category == categoria )
            
            showCard(filtered)

        } else {

     

           showCard(data)

           

        }


    }

 let checkInput = document.querySelectorAll('.form-check-input')
checkInput.forEach((checkInput) => {

    checkInput.addEventListener('click', ()=> {

        filteredByCategory (checkInput.id) 

    })


})


}) 

